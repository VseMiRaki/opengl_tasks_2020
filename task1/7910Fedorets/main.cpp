#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>


#include <iostream>
#include <vector>
#include "PerlinNoise.hpp"
#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

glm::vec3 GetTriangleNormal (const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3) {
    return glm::cross(p3 - p2, p3 -  p1);
}



MeshPtr MakeTerrain(int length, int width, int height, int size = 30) {
    PerlinNoise noise;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    std::vector<std::vector<glm::vec3>> points(length * size + 1, std::vector<glm::vec3>(width * size + 1));
    std::vector<std::vector<glm::vec3>> pointsNormals(length * size + 1, std::vector<glm::vec3>(width * size + 1));

    float max = 0;

    for (int i = 0; i < length * size + 1; ++i) {
        for (int j = 0; j < width * size + 1; ++j) {
            auto x = static_cast<float>(i);
            auto y = static_cast<float>(j);

            points[i][j] = {x / length * size, y / width * size, (noise.GetNoise(x / length, y / width) + 1.0) / 2.0 * height};
            max = std::max(max, std::abs((noise.GetNoise(x / length, y / width) + 1.0f) / 2.0f * height));
        }
    }

    for (int i = 0; i < length * size; ++i) {
        for (int j = 0; j < width * size; ++j) {
            if (i == 0 || j == 0) {
                continue;
            }

            auto p1 = points[i - 1][j - 1];
            auto p2 = points[i - 1][j];
            auto p3 = points[i][j + 1];
            auto p4 = points[i + 1][j + 1];
            auto p5 = points[i + 1][j];
            auto p6 = points[i][j - 1];
            auto p7 = points[i][j];

            glm::vec3 normal;

            normal += GetTriangleNormal(p7, p1, p2);
            normal += GetTriangleNormal(p7, p2, p3);
            normal += GetTriangleNormal(p7, p3, p4);
            normal += GetTriangleNormal(p7, p4, p5);
            normal += GetTriangleNormal(p7, p5, p6);
            normal += GetTriangleNormal(p7, p6, p1);

            normal /= 6.0;

            pointsNormals[i][j] = normal;
        }
    }

    for (int i = 0; i < length * size; ++i) {
        for (int j = 0; j < width * size; ++j) {
            //inserting 2 triangles with their normals
            vertices.push_back(points[i][j]);
            vertices.push_back(points[i][j + 1]);
            vertices.push_back(points[i + 1][j + 1]);

            vertices.push_back(points[i][j]);
            vertices.push_back(points[i + 1][j]);
            vertices.push_back(points[i + 1][j + 1]);


            normals.push_back(pointsNormals[i][j]);
            normals.push_back(pointsNormals[i][j + 1]);
            normals.push_back(pointsNormals[i + 1][j + 1]);

            normals.push_back(pointsNormals[i][j]);
            normals.push_back(pointsNormals[i + 1][j]);
            normals.push_back(pointsNormals[i + 1][j + 1]);
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Terrain is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class SampleApplication : public Application {
public:
    MeshPtr terrainMesh;
    ShaderProgramPtr terrainShader;

    void makeScene() override {
        Application::makeScene();

        terrainMesh = MakeTerrain(20, 20, 20);
        terrainMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, -10.0f, -20)));

        terrainShader = std::make_shared<ShaderProgram>("7910FedoretsData1/shader.vert", "7910FedoretsData1/shader.frag");
        _cameraMover = std::make_shared<FreeCameraMover>();
    }

    void draw() override {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        terrainShader->use();
        terrainShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        terrainShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        terrainShader->setMat4Uniform("modelMatrix", terrainMesh->modelMatrix());

        terrainMesh->draw();
    }

};


int main()
{
    SampleApplication app;
    app.start();



    return 0;
}