//
// Created by Nikita Fedorets on /16/421.
//

#include "PerlinNoise.hpp"
#include <cmath>

float PerlinNoise::dotGridGradient(const Point2D& a, const Point2D& b) {
    return (a - b).Dot(randomGradient2D(a));
}

float PerlinNoise::interpolate(float a0, float a1, float w) {
    return (a1 - a0) * ((w * (w * 6.0 - 15.0) + 10.0) * w * w * w) + a0;
//    return (a1 - a0) * (3.0 - w * 2.0) * w * w + a0;
//    return (a1 - a0) * w + a0;
}

PerlinNoise::Point2D PerlinNoise::randomGradient2D(const Point2D& a) {
    float random = 2920.f * sin(a.x * 21942.f + a.y * 171324.f + 8912.f) * cos(a.x * 23157.f * a.y * 217832.f + 9758.f);
    return { cos(random), sin(random) };
}

float PerlinNoise::GetNoise(float x, float y) {
    float x0 = static_cast<int>(x);
    float x1 = x0 + 1;
    float y0 = static_cast<int>(y);
    float y1 = y0 + 1;

    float sx = x - (long)x;
    float sy = y - (long)y;

    float ix0 = interpolate(dotGridGradient({x0, y0}, {x, y}), dotGridGradient({x1, y0}, {x, y}), sx);
    float ix1 = interpolate(dotGridGradient({x0, y1}, {x, y}), dotGridGradient({x1, y1}, {x, y}), sx);

    return interpolate(ix0, ix1, sy);
}