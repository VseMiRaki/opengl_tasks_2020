//
// Created by Nikita Fedorets on /16/421.
//

#pragma once


#include <random>

class PerlinNoise {

    struct Point2D {
        float x;
        float y;

        Point2D operator-(const Point2D& other) const {
            return {x - other.x, y - other.y};
        }

        float Dot(const Point2D& other) const {
            return x * other.x + y * other.y;
        }
    };

public:

    float GetNoise(float x, float y);

private:
    float interpolate(float a0, float a1, float w);

    Point2D randomGradient2D(const Point2D& a);

    float dotGridGradient(const Point2D& a, const Point2D& b);
};
